package com.akg.sensor.watergauge.bluno;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Spinner;
import android.widget.TextView;

import com.akg.sensor.watergauge.utils.LineGraphDrawer;
import com.akg.sensor.watergauge.utils.MathUtils;
import com.example.blunobasicdemo.R;

public class MainActivity extends BlunoLibrary {
	// View
	private Button buttonScan;
	private Button btnMin;
	private Button btnMax;
	private Spinner spnDeepest;
	private LinearLayout barGraphWhite;
	private LinearLayout barGraphBlue;
	private ImageView lineGraph;
	
	private double mCurCapacVal; // raw data
	private double mCurDeep; // mapped data
	private ArrayList<Double> mCurDeepHistory; // history of mapped data
	private final int SIZE_OF_HISTORY = 25;
	
	private float MIN = 0, MAX = 5000, SHALLOWEST = 0, DEEPEST = 20, RESOLUTION = 0.5f;
	private List<Integer> spinnerList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		onCreateProcess(); // onCreate Process by BlunoLibrary
		serialBegin(115200); // set the Uart Baudrate on BLE chip to 115200
		
		// initialize
		mCurDeepHistory = new ArrayList<Double>();
		for (int i=0; i<SIZE_OF_HISTORY; i++) {
			mCurDeepHistory.add(0.0);
		}
		
		spnDeepest = (Spinner) findViewById(R.id.spnDeepest);
		buttonScan = (Button) findViewById(R.id.buttonScan);
		btnMin = (Button) findViewById(R.id.btnMin);
		btnMax = (Button) findViewById(R.id.btnMax);
		barGraphWhite = (LinearLayout) findViewById(R.id.barGraphWhite);
		barGraphBlue = (LinearLayout) findViewById(R.id.barGraphBlue);
		lineGraph = (ImageView) findViewById(R.id.lineGraph);

		// Initialize the Views
		
		spnDeepest.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				DEEPEST = spinnerList.get(position);
				
				// Adjust the ticks
				LinearLayout tickLayout = (LinearLayout) findViewById(R.id.tickLayout);
				int counter = 10;
				for (int i=0; i<tickLayout.getChildCount(); i++) {
					View v = tickLayout.getChildAt(i);
					if (v instanceof TextView) {
						((TextView) v).setText("" + DEEPEST*counter/10);
						counter--;
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});
		addItemsOnSpinnerDeepest();

		buttonScan.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Show Dialog for selecting BLE devices
				buttonScanOnClickProcess();  
			}
		});

		btnMin.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MIN = (float) mCurCapacVal;
				btnMin.setText("Min\n" + MIN);
			}
		});

		btnMax.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MAX = (float) mCurCapacVal;
				btnMax.setText("Max\n" + MAX);
			}
		});
	}

	protected void onResume() {
		super.onResume();
		System.out.println("BlUNOActivity onResume");
		onResumeProcess(); // onResume Process by BlunoLibrary
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		onActivityResultProcess(requestCode, resultCode, data); // processed by BlunoLibrary
		
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onPause() {
		super.onPause();
		onPauseProcess(); // onPause Process by BlunoLibrary
	}

	protected void onStop() {
		super.onStop();
		onStopProcess(); // onStop Process by BlunoLibrary
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		onDestroyProcess(); // onDestroy Process by BlunoLibrary
	}

	/**
	 * Once connection state changes, this function will be called
	 */
	@Override
	public void onConectionStateChange(connectionStateEnum theConnectionState) {
		switch (theConnectionState) { // Four connection state
		case isConnected:
			buttonScan.setText("Connected");
			break;
		case isConnecting:
			buttonScan.setText("Connecting");
			break;
		case isToScan:
			buttonScan.setText("Scan");
			break;
		case isScanning:
			buttonScan.setText("Scanning");
			break;
		case isDisconnecting:
			buttonScan.setText("isDisconnecting");
			break;
		default:
			break;
		}
	}

	/**
	 * Once connection data received, this function will be called
	 */
	@Override
	public void onSerialReceived(String theString) {
		try {
			mCurCapacVal = Double.parseDouble(theString);
			Log.d("mCurCapacVal", "" + mCurCapacVal);
			mCurDeep = MathUtils.mapWaterLevel((float) mCurCapacVal, MIN, MAX, SHALLOWEST, DEEPEST, RESOLUTION);
			
			// add the data into the history
			mCurDeepHistory.add(mCurDeep);
			// remove too old data from the history
			while (mCurDeepHistory.size() > SIZE_OF_HISTORY) {
				mCurDeepHistory.remove(0);
			}
			
			buttonScan.setText("Current\n" + mCurDeep);
			
			// draw the bar graph (using a simple layout)
			LayoutParams params = (LayoutParams) barGraphBlue.getLayoutParams();
			params.weight = (float) mCurDeep;
			barGraphBlue.setLayoutParams(params);
			params = (LayoutParams) barGraphWhite.getLayoutParams();
			params.weight = (float) (DEEPEST-mCurDeep);
			barGraphWhite.setLayoutParams(params);
			
			// draw the line graph
			int width = lineGraph.getMeasuredWidth();
			int height = lineGraph.getMeasuredHeight();
			Bitmap graph = LineGraphDrawer.draw(mCurDeepHistory, DEEPEST, width, height, this);
			lineGraph.setImageBitmap(graph);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 *  add items into spinner dynamically
	 */
	public void addItemsOnSpinnerDeepest() {
		spinnerList = new ArrayList<Integer>();
		for (int i = 0; i < 100; i++) {
			spinnerList.add(i + 1);
		}
		ArrayAdapter<Integer> dataAdapter = new ArrayAdapter<Integer>(this,
				android.R.layout.simple_spinner_item, spinnerList);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spnDeepest.setAdapter(dataAdapter);
		spnDeepest.setSelection(19);
	}

}