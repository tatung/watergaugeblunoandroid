package com.akg.sensor.watergauge.utils;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public class LineGraphDrawer {
	public static final int PERIOD_MINUTE = 0;
	public static final int PERIOD_HOUR = 1;
	public static final int PERIOD_DAY = 2;
	
	public static Bitmap draw(ArrayList<Double> data, float max, int width, int height, Context context){
		// prepare the canvas
		Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_4444);
		Canvas canvas = new Canvas(bitmap);
		
		// prepare the pen
		Paint bluePen = new Paint();
		bluePen.setColor(Color.rgb(0, 127, 255));
		bluePen.setStrokeWidth(5.0f);
		
		// drawgraph
		float lastX = 0.0f;
		float lastY = 0.0f;
		for(int i=0; i<data.size(); i++){
			float x = width * i / (data.size() - 1);
			float y = (float) ( height * (max - data.get(i) ) / max);
			canvas.drawCircle(x, y, 10, bluePen);
			
			if (i > 0) {
				canvas.drawLine(lastX, lastY, x, y, bluePen);
			}
			lastX = x;
			lastY = y;
		}
		
		return bitmap;
	}
}