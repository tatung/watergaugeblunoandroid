package com.akg.sensor.watergauge.utils;

public class MathUtils {
	public static float mapWaterLevel(float val, float oMin, float oMax,
			float nMin, float nMax, float resolution) {
		float scale = resolution * (oMax - oMin) / (nMax - nMin);
		return (float) (resolution * ((int) (val / scale + 0.5)));
	}

}
